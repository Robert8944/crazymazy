import java.awt.*;

import javax.swing.*;
public class MazeCell extends JPanel{

	private static final long serialVersionUID = 1L;
	private boolean[] borders;
	public static final int UP = 0, RIGHT = 1, DOWN=2, LEFT = 3;
	public static final int BLANK=0, VISITED=1, DEAD=2, ON=3;
	private Color[] colors = {Color.WHITE, Color.BLUE, Color.WHITE};
	public boolean isHome;
	public static Image person= Toolkit.getDefaultToolkit().getImage("./face.gif");;
	private static MediaTracker mt = new MediaTracker(new Component(){
		private static final long serialVersionUID = 1L;});
	
	private int status;
	private int row, col;
	public MazeCell(int r, int c){
		super();
		
		mt.addImage(person,0);
		try{mt.waitForID(0);}catch(Exception ex){System.out.println("FAIL");}
		
		isHome=false;
		row =r;
		col = c;
		borders = new boolean[4];
		for(int i=0; i<4; i++)
			borders[i] = true;
		this.setBackground(Color.WHITE);
		status = BLANK;
	}

	public int row(){return row;}
	public int col(){return col;}
	
	public void setHome(boolean i){
		isHome = i;
	}
	public void setStatus(int x){
		status = x;
		this.setBackground(colors[status]);
		this.paint(this.getGraphics());
	}
	public int getStatus(){
		return status;
	}
	public boolean isBlank(){return status==BLANK;}
	public boolean isVisited(){return status==VISITED;}
	public boolean isDead(){return status==DEAD;}
	public boolean isOn(){return status==ON;}
	
	public boolean blocked(int dir){return borders[dir];}
	
	public void clearUp(){borders[UP] = false; repaint();}
	public void clearDown(){borders[DOWN] = false; repaint();}
	public void clearRight(){borders[RIGHT] = false; repaint();}
	public void clearLeft(){borders[LEFT] = false; repaint();}
	
	public void paintComponent(Graphics g){
		super.paintComponent(g);
		if(isHome){
			g.drawImage(person,0,0,this.getWidth(),this.getHeight(),null);
		
		}
		//g.drawString("("+row+","+col+")",15,15);
		g.setColor(Color.BLACK);
		if(borders[UP])
			g.drawLine(0,0,this.getWidth(),0);
		if(borders[RIGHT])
			g.drawLine(this.getWidth(),0,this.getWidth(),this.getHeight());
		if(borders[DOWN])
			g.drawLine(0,this.getHeight(),this.getWidth(),this.getHeight());
		if(borders[LEFT])
			g.drawLine(0,0,0,this.getHeight());
		if(!isHome){
			g.setColor(colors[status]);
		}
		//g.fillRect(this.getWidth(),this.getHeight(),this.getWidth(),this.getHeight() );
		
		
	}

}

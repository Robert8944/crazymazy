import javax.swing.*;
import javax.swing.Timer;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.*;
public class MazeFrame extends JFrame implements ActionListener, KeyListener{
	private static final long serialVersionUID = 1L;
	private MazeCell[][] grid;
	private Stack<MazeCell> SolveStack, BustStack, MoveStack;
	private ArrayList<MazeCell> possibleMoves;
	public JButton solve, reset, newMaze;
	public JPanel setUp, setUp2;
	public boolean isFirstBust, isSolved=false;
	public MazeCell toBeAdded, firstCell, lastCell;
	public int rows, cols, speedForNewMaze;
	public JSlider speedSetter;
	public Timer toAnimate;
	
	//constructor!!!
	public MazeFrame(){
		super("Crazy Mazey");
		rows = Integer.parseInt(JOptionPane.showInputDialog(this,"Rows:"));
		cols = Integer.parseInt(JOptionPane.showInputDialog(this,"Colums:"));
		isFirstBust = true;
		

		grid = new MazeCell[rows][cols];
		SolveStack = new Stack<MazeCell>();
		BustStack = new Stack<MazeCell>();
		MoveStack = new Stack<MazeCell>();
		possibleMoves = new ArrayList<MazeCell>();
		speedSetter = new JSlider(0,100);
		speedSetter.setValue(5);
		speedSetter.setMajorTickSpacing(10);
		speedSetter.setMinorTickSpacing(2);
		speedSetter.setPaintTicks(true);
		speedSetter.setPaintLabels(true);
		speedSetter.setSnapToTicks(true);
		speedSetter.setBorder(BorderFactory.createTitledBorder("Speed, in moves per second"));
		speedSetter.setFocusable(false);
		toAnimate = new Timer(5, new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				step();
				
			}
			
		});
		
		solve = new JButton("solve");
		solve.setFocusable(false);
		solve.addActionListener(this);
		reset = new JButton("reset");
		reset.setFocusable(false);
		reset.addActionListener(this);
		newMaze = new JButton("new Maze");
		newMaze.setFocusable(false);
		newMaze.addActionListener(this);
		
		//put stuff on there
		setUp = new JPanel(new GridLayout(rows,cols));
		setUp2 = new JPanel(new GridLayout(1,4));
		
		
		setUp2.add(solve);
		setUp2.add(reset);
		setUp2.add(newMaze);
		setUp2.add(speedSetter);
		
		//put your cells onto the panel
		
		for(int i = 0; i<rows; i++)
			for(int j = 0; j<cols; j++){
				grid[i][j] = new MazeCell(i,j);
				setUp.add(grid[i][j]);
				if (j == 0){
					grid[i][j].setStatus(2);
					grid[i][j].clearUp();
					grid[i][j].clearDown();
					grid[i][j].clearLeft();
				}
				if (j == cols-1){
					grid[i][j].setStatus(2);
					grid[i][j].clearUp();
					grid[i][j].clearDown();
					grid[i][j].clearRight();
				}
			}
		
		setUp.addKeyListener(this);
		setUp.setFocusable(true);
		setUp.requestFocusInWindow();
		setUp.grabFocus();
		setUp.requestFocus(false);
		
		/*
		DarthVaderPez.push(grid[2][0]);
		
		grid[2][0].clearRight();
		grid[2][1].clearLeft();
		grid[2][1].clearRight(); 
		grid[2][2].clearLeft(); 
		for(int i=2; i<7;i++){
			grid[i][2].clearDown();
			grid[i+1][2].clearUp();
		}
		grid[6][2].clearRight();
		grid[6][3].clearLeft();
		grid[6][3].clearRight();
		grid[6][4].clearLeft();
		grid[6][4].clearRight();
		grid[6][5].clearLeft();
		grid[6][5].clearRight();
		grid[6][6].clearLeft();
		grid[6][6].clearUp();
		grid[5][6].clearUp();
		
		for(int i=5; i>2;i--){
			grid[i][6].clearDown();
			grid[i-1][6].clearUp();
		}
		grid[1][6].clearLeft();
		for(int i=5; i>2;i--){
			grid[1][i].clearRight();
			grid[1][i-1].clearLeft();
		}
		grid[4][6].clearRight();
		grid[4][7].clearLeft();
		for(int i=4; i<9;i++){
			grid[i][7].clearDown();
			grid[i+1][7].clearUp();
		}
		grid[1][1].clearRight();
		grid[1][2].clearRight();
		grid[8][2].clearUp();
		grid[7][2].clearDown();
		grid[1][6].clearDown();
		grid[2][6].clearDown();
		grid[1][5].clearLeft();
		grid[9][7].clearRight();
		grid[9][8].clearLeft();
		grid[9][8].clearRight();
		grid[9][9].clearLeft();
		*/
		firstCell = grid[(int)(Math.random()*(rows-1))][0];
		BustStack.push(firstCell);
		SolveStack.push(BustStack.peek());
		MoveStack.push(BustStack.peek());
		MoveStack.peek().isHome = true;
		for (int i = 0; i<rows; i++){
			grid[i][0].setStatus(2);
		}
		BustStack.peek().setStatus(1);
		for (int i = 0; i<rows; i++){
			grid[i][cols-1].setStatus(2);
		}
		lastCell = grid[(int)(Math.random()*(rows-1))][cols-1];
		lastCell.setStatus(0);
		isFirstBust = false;
		while(this.canBust()){
			this.bust();
			
		}
		//if(BustStack.peek().col() == 0 && isFirstBust == false){
		for(int i = 0; i<rows; i++)
			for(int j = 0; j<cols; j++)
				grid[i][j].setStatus(0);
		for (int i = 0; i<rows; i++){
			grid[i][cols-1].setStatus(2);
		}
		for (int i = 0; i<rows; i++){
			grid[i][0].setStatus(2);
		}
		lastCell.setStatus(0);
		firstCell.setStatus(1);
		
		//}
		//put the panel on the frame
		this.add(setUp2, BorderLayout.NORTH);
		this.add(setUp, BorderLayout.CENTER);
		
				
		//finish up
		this.setSize(800,800);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setVisible(true);
	}//end constructor

	
	public static void main(String[] args) {
		new MazeFrame();
	}
	
	public boolean canBust(){
		if(BustStack.isEmpty())
			return false;
		if(BustStack.peek().col()-1>-1 && BustStack.peek().col()+1<cols && BustStack.peek().row()-1>-1 && BustStack.peek().row()+1 < rows && grid[BustStack.peek().row()+1][BustStack.peek().col()].getStatus()==2 && grid[BustStack.peek().row()-1][BustStack.peek().col()].getStatus()==2 && grid[BustStack.peek().row()][BustStack.peek().col()+1].getStatus()==2 && grid[BustStack.peek().row()][BustStack.peek().col()-1].getStatus()==2){
			return false;
		}
		return true;
	}
	
	public void bust(){
		
		
		
			if(BustStack.peek().row()+1 < rows && grid[BustStack.peek().row()+1][BustStack.peek().col()].getStatus() == 0){
				
				possibleMoves.add(grid[BustStack.peek().row()+1][BustStack.peek().col()]);
				
				
			}
			if(BustStack.peek().col()+1 < cols && grid[BustStack.peek().row()][BustStack.peek().col()+1].getStatus() == 0){
				
				possibleMoves.add(grid[BustStack.peek().row()][BustStack.peek().col()+1]);
				
			}
			if(BustStack.peek().row()-1 > -1 && grid[BustStack.peek().row()-1][BustStack.peek().col()].getStatus() == 0){
				
				possibleMoves.add(grid[BustStack.peek().row()-1][BustStack.peek().col()]);
			
			}
			if(BustStack.peek().col()-1 > 0 && grid[BustStack.peek().row()][BustStack.peek().col()-1].getStatus() == 0){
				
				possibleMoves.add(grid[BustStack.peek().row()][BustStack.peek().col()-1]);
				
			}
			if(possibleMoves.size()>0){
				toBeAdded = (possibleMoves.get((int)(Math.random()*possibleMoves.size())));
				if(toBeAdded.col()== BustStack.peek().col()+1){
					BustStack.peek().clearRight();
					BustStack.push(toBeAdded);
					BustStack.peek().clearLeft();
					
				}
				else if(toBeAdded.col()== BustStack.peek().col()-1){
					BustStack.peek().clearLeft();
					BustStack.push(toBeAdded);
					BustStack.peek().clearRight();
				}
				else if(toBeAdded.row()== BustStack.peek().row()+1){
					BustStack.peek().clearDown();
					BustStack.push(toBeAdded);
					BustStack.peek().clearUp();
				}
				else if(toBeAdded.row()== BustStack.peek().row()-1){
					BustStack.peek().clearUp();
					BustStack.push(toBeAdded);
					BustStack.peek().clearDown();
				}
				
				
				BustStack.peek().setStatus(1);
				
			}
			else{
				/*
				if(BustStack.peek().row()+1 < 10 && grid[BustStack.peek().row()+1][BustStack.peek().col()].getStatus() != 2 && BustStack.peek().blocked(MazeCell.DOWN)){
					possibleMoves.add(grid[BustStack.peek().row()+1][BustStack.peek().col()]);
					BustStack.peek().setStatus(2);
				}
				if(BustStack.peek().col()+1 < 10 && grid[BustStack.peek().row()][BustStack.peek().col()+1].getStatus() != 2 && BustStack.peek().blocked(MazeCell.RIGHT)){
					possibleMoves.add(grid[BustStack.peek().row()][BustStack.peek().col()+1]);
					BustStack.peek().setStatus(2);
				}
				if(BustStack.peek().row()-1 > 0 && grid[BustStack.peek().row()-1][BustStack.peek().col()].getStatus() != 2 && BustStack.peek().blocked(MazeCell.UP)){
					possibleMoves.add(grid[BustStack.peek().row()-1][BustStack.peek().col()]);
					BustStack.peek().setStatus(2);
				}
				if(BustStack.peek().col()-1 > 0 && grid[BustStack.peek().row()][BustStack.peek().col()-1].getStatus() != 2 && BustStack.peek().blocked(MazeCell.LEFT)){
					possibleMoves.add(grid[BustStack.peek().row()][BustStack.peek().col()-1]);
					BustStack.peek().setStatus(2);
				}
				*/
				BustStack.pop().setStatus(2);
				
				//BustStack.peek().setStatus(1);
			}
			
		
		while(!possibleMoves.isEmpty()){
			possibleMoves.remove(0);
		}
	}
	
	public void step(){
		if(!isSolved){
		
		if (!SolveStack.peek().blocked(MazeCell.UP) && !grid[SolveStack.peek().row()-1][SolveStack.peek().col()].isVisited() && !grid[SolveStack.peek().row()-1][SolveStack.peek().col()].isDead()){
			SolveStack.push(grid[SolveStack.peek().row()-1][SolveStack.peek().col()]);
			SolveStack.peek().setStatus(1);
			
		}
		
		else if (!SolveStack.peek().blocked(MazeCell.RIGHT) && !grid[SolveStack.peek().row()][SolveStack.peek().col()+1].isVisited() && !grid[SolveStack.peek().row()][SolveStack.peek().col()+1].isDead()){
			SolveStack.push(grid[SolveStack.peek().row()][SolveStack.peek().col()+1]);
			SolveStack.peek().setStatus(1);
			
		}
		
		else if (!SolveStack.peek().blocked(MazeCell.DOWN) && !grid[SolveStack.peek().row()+1][SolveStack.peek().col()].isVisited() && !grid[SolveStack.peek().row()+1][SolveStack.peek().col()].isDead()){
			SolveStack.push(grid[SolveStack.peek().row()+1][SolveStack.peek().col()]);
			SolveStack.peek().setStatus(1);
		}
		
		else if (!SolveStack.peek().blocked(MazeCell.LEFT) && !grid[SolveStack.peek().row()][SolveStack.peek().col()-1].isVisited() && !grid[SolveStack.peek().row()][SolveStack.peek().col()-1].isDead()){
			SolveStack.push(grid[SolveStack.peek().row()][SolveStack.peek().col()-1]);
			SolveStack.peek().setStatus(1);
		}
		
		
		
		
		
		else if (!SolveStack.peek().blocked(MazeCell.UP) && !grid[SolveStack.peek().row()-1][SolveStack.peek().col()].isDead()){
			SolveStack.peek().setStatus(2);
			SolveStack.push(grid[SolveStack.peek().row()-1][SolveStack.peek().col()]);
			
			
		}
		
		else if (!SolveStack.peek().blocked(MazeCell.RIGHT) && !grid[SolveStack.peek().row()][SolveStack.peek().col()+1].isDead()){
			SolveStack.peek().setStatus(2);
			SolveStack.push(grid[SolveStack.peek().row()][SolveStack.peek().col()+1]);
			
			
		}
		
		else if (!SolveStack.peek().blocked(MazeCell.DOWN) && !grid[SolveStack.peek().row()+1][SolveStack.peek().col()].isDead()){
			SolveStack.peek().setStatus(2);
			SolveStack.push(grid[SolveStack.peek().row()+1][SolveStack.peek().col()]);
			
		}
		
		else if (!SolveStack.peek().blocked(MazeCell.LEFT) && !grid[SolveStack.peek().row()][SolveStack.peek().col()-1].isDead()){
			SolveStack.peek().setStatus(2);
			SolveStack.push(grid[SolveStack.peek().row()][SolveStack.peek().col()-1]);
			
		}
		
			if(SolveStack.peek().col()==cols-1){
				toAnimate.stop();
				isSolved = true;
				JOptionPane.showMessageDialog(this,"Your solver is Done");
			}
			if(speedSetter.getValue()!=0)
				toAnimate.setDelay((int)(1.0/(double)speedSetter.getValue()*1000));
				//toAnimate.setDelay(0);
			else{
				speedSetter.setValue(1);
				toAnimate.setDelay((int)(1.0/(double)speedSetter.getValue()*1000));
			}
			
		
		}
		else{
			JOptionPane.showMessageDialog(this,"You have to reset first...");
			toAnimate.stop();
		}
		
		
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		
		if(arg0.getSource()==(solve)){
			toAnimate.start();
		}
		if(arg0.getSource()==(reset)){
			for(int i = 0; i<rows; i++)
				for(int j = 0; j<cols; j++){
					grid[i][j].setStatus(0);
				}
			while(SolveStack.size()!=1){
				SolveStack.pop();
			}
			SolveStack.peek().setStatus(1);
			isSolved = false;
			toAnimate.stop();
			for (int i = 0; i<rows; i++){
				grid[i][cols-1].setStatus(2);
			}
			for (int i = 0; i<rows; i++){
				grid[i][0].setStatus(2);
			}
			lastCell.setStatus(0);
			SolveStack.peek().setStatus(1);
			
			
			
		
		}
		if(arg0.getSource().equals(newMaze)){
			speedForNewMaze=speedSetter.getValue();
			this.dispose();
			new MazeFrame();
			speedSetter.setValue(speedForNewMaze);
		}
		
	}


	@Override
	public void keyPressed(KeyEvent arg0) {
		if (arg0.getKeyCode() == KeyEvent.VK_LEFT && !MoveStack.peek().blocked(MazeCell.LEFT) && MoveStack.peek().col()-1>-1){
			MoveStack.peek().setHome(false);
			MoveStack.push(grid[MoveStack.peek().row()][MoveStack.peek().col()-1]);
			MoveStack.peek().setHome(true);
			this.repaint();
		}
		if (arg0.getKeyCode() == KeyEvent.VK_RIGHT && !MoveStack.peek().blocked(MazeCell.RIGHT) && MoveStack.peek().col()+1<cols){
			
			MoveStack.peek().setHome(false);
			MoveStack.push(grid[MoveStack.peek().row()][MoveStack.peek().col()+1]);
			MoveStack.peek().setHome(true);
			this.repaint();
			if(MoveStack.peek().col()+1==cols){
				JOptionPane.showMessageDialog(this,"you've reached the finish!");
				MoveStack.peek().isHome =false;
				while(MoveStack.size()!=1){
					MoveStack.pop();
				}
				MoveStack.peek().isHome = true;
				this.repaint();
			}
		}
		if (arg0.getKeyCode() == KeyEvent.VK_UP && !MoveStack.peek().blocked(MazeCell.UP) && MoveStack.peek().row()-1>-1){
			MoveStack.peek().setHome(false);
			MoveStack.push(grid[MoveStack.peek().row()-1][MoveStack.peek().col()]);
			MoveStack.peek().setHome(true);
			this.repaint();
		}
		if (arg0.getKeyCode() == KeyEvent.VK_DOWN && !MoveStack.peek().blocked(MazeCell.DOWN) && MoveStack.peek().row()+1<rows){
			MoveStack.peek().setHome(false);
			MoveStack.push(grid[MoveStack.peek().row()+1][MoveStack.peek().col()]);
			MoveStack.peek().setHome(true);
			this.repaint();
		}
	}


	@Override
	public void keyReleased(KeyEvent arg0) {
		
	}


	@Override
	public void keyTyped(KeyEvent arg0) {
		
	}


	

}



public class MyLinkedList {
	private ListNode firstNode;
	
	public MyLinkedList(){
		firstNode=null;
	}
	public boolean isEmpty(){return firstNode==null;}
//more code
	public void addFirst(Object toAdd){
		ListNode second = firstNode;
		firstNode = new ListNode(toAdd, second);
	}
	public Object removeFirst(){
		if (firstNode==null){
			return null;
		}
		Object ans = firstNode.getValue();
		firstNode=firstNode.getNext();
		return ans;
	}
	public String toString(){
		String ans = "";
		ListNode current = firstNode;
		while(current!=null){
			ans+=current.getValue()+" ";
			current=current.getNext();
		}
		return ans;
	}
	public int size(){
		int ans= 0;
		ListNode current = firstNode;
		while(current!=null){
			ans+=1;
			current=current.getNext();
		}
		return ans;
	}
	public boolean contains(Object toFind){
		
		ListNode current = firstNode;
		while(current!=null){
			if (current.getValue().equals(toFind))
				return true;
			current=current.getNext();
		}
		return false;
	}
	public Object get(int index){
		if(size()<=index || index<0)
			throw new ArrayIndexOutOfBoundsException();
		ListNode current = firstNode;
		for(int i=0; i<index; i++)
			current = current.getNext();
		return current.getValue();
	}
	public Object set(int index, Object newVal){
		Object ans=null;
		if(size()<=index || index<0)
			throw new ArrayIndexOutOfBoundsException();
		ListNode current = firstNode;
		for(int i=0; i<index; i++)
			current = current.getNext();
		ans = current.getValue();
		current.setValue(newVal);
		return ans;
	}
	public boolean add(Object toAdd){
		if (firstNode==null){
			addFirst(toAdd);
			return true;
		}
		
		ListNode current = firstNode;
		while(current.getNext()!=null)
			current = current.getNext();
		current.setNext(new ListNode(toAdd, null));
		return true;
	}
	public boolean add(int index, Object toAdd){
		if(index==0){
			addFirst(toAdd);
			return true;
		}
		
		
		ListNode current = firstNode;
		for(int i = 0; i<index-1; i++){
			if(current==null){
				throw new ArrayIndexOutOfBoundsException();
			}
			current = current.getNext();
		}
		current.setNext(new ListNode(toAdd, current.getNext()));
		
		
		
		
		return true;
		
	}
	public Object remove(int index){
		if(index == 0){
			Object ans = firstNode.getValue();
			this.removeFirst();
			return ans;
		}
		
		ListNode current = firstNode;
		for(int i = 0; i<index-1; i++){
			if(current==null){
				throw new ArrayIndexOutOfBoundsException();
			}
			current = current.getNext();
		}
		Object ans = current.getNext().getValue();
		current.setNext(current.getNext().getNext());
		return ans;
		
	}
	public boolean remove(Object target){
		
		
		ListNode current = firstNode;
		while(!current.getNext().equals(null) && !current.getValue().equals(target)){
			if(current.getValue()==target)
				this.remove(current);
			current = current.getNext();
		}
		return true;
	}
}

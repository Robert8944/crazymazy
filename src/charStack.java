
public class charStack{
	MyLinkedList bob = new MyLinkedList();
	public charStack(){
		
	}
	public char pop(){
		return ((Character)(bob.remove(bob.size()-1))).charValue();
	}
	public char peek(){
		return ((Character)(bob.get(bob.size()-1))).charValue();
	}
	public void push(char c){
		bob.add(new Character(c));
	}
	public boolean isEmpty(){
		if(bob.size()==0)
			return true;
		else
			return false;
		
	}
	public int size(){
		return bob.size();
	}

}
